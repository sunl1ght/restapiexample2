import { combineReducers, createStore, applyMiddleware } from 'redux';
import { reducer as formReducer } from 'redux-form';
import thunk from 'redux-thunk';

import employeesReducer from './employees';

const rootReducer = combineReducers({
	'employees': employeesReducer,
	'form': formReducer
});

export const Store = createStore(rootReducer, applyMiddleware(thunk));
const initialState = {};
var _ = require('lodash');
let newState = {};

export default function reducer (state = initialState, action) {
    switch (action.type) {
        case 'APPEND_EMPLOYEES':
            newState = {};
            action.payload.map(item => newState[item.id] = item);
            return newState;
        case 'DELETE_EMPLOYEE':
            newState = { ...state };
            return _.omit(newState, [action.payload]);
        case 'ADD_EMPLOYEE':
            newState = { ...state, [action.payload.id]: action.payload };
            return newState;
        case 'UPDATE_EMPLOYEE':
            newState = { ...state };
            newState[action.payload.id] = {...state[action.payload.id], ...action.payload.data};
            return newState;
            // return state;
        default:
            return state;
    }
}
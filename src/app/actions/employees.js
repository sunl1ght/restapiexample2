export function employeesFunc() {
	return (dispatch) => {
		return fetch('http://dummy.restapiexample.com/api/v1/employees', {
			'method': 'GET',
			'headers': {'Accept': 'application/json, application/xml, text/plain, text/html, *.*'}
		}).then(res => res.json())
		.then(res => {
			dispatch({
				'payload': res,
				'type': 'APPEND_EMPLOYEES'
			});
		})
	};
}
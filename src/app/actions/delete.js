export function deleteFunc(id) {
	return (dispatch) => {
		return fetch('http://dummy.restapiexample.com/api/v1/delete/' + id, {
			'method': 'DELETE',
			'headers': {
				'Accept': 'application/json, application/xml, text/plain, text/html, *.*'
			}
		}).then(res => res.json())
		.then(res => {
			dispatch({
				'payload': id,
				'type': 'DELETE_EMPLOYEE'
			});
		})
	  };

}
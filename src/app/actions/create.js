export function createFunc(data) {
	return (dispatch) => {
		return fetch('http://dummy.restapiexample.com/api/v1/create', {
			'method': 'POST',
			'headers': {
				'Accept': 'application/json, application/xml, text/plain, text/html, *.*'
			},
			'body': JSON.stringify(data)
		}).then(res => res.json())
		.then(res => {
			let newData = {
				'employee_name': res['name'],
				'employee_salary': res['salary'],
				'employee_age': res['age']
			};

			dispatch({
				'payload': { ...newData, ...res },
				'type': 'ADD_EMPLOYEE'
			});
		})
	};
}
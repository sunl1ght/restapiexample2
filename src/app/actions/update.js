export function updateFunc(data) {
	const newData = {
		'name': data['employee_name'],
		'salary': data['employee_salary'],
		'age': data['employee_age']
	};
	return (dispatch, getState) => {
		return fetch('http://dummy.restapiexample.com/api/v1/update/' + data['id'], {
			'method': 'PUT',
			'headers': {
				'Accept': 'application/json, application/xml, text/plain, text/html, *.*'
			},
			'body': JSON.stringify(newData)
		}).then(res => res.json())
		.then(res => {
			res['employee_name'] = res['name'];
			res['employee_salary'] = res['salary'];
			res['employee_age'] = res['age'];
			dispatch({
				'payload': {
					'id': data['id'],
					'data': {...res}
				},
				'type': 'UPDATE_EMPLOYEE'
			});
		})
	  };

}
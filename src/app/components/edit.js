import React from 'react';
import Button from 'react-bootstrap/Button';

import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import EditForm from './editForm';

class Component extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
      'modal': false
		};
	}

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
    return (
      <div>
        <Button onClick={this.toggle}>Edit</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader>Edit {this.props.itemId}</ModalHeader>
          <ModalBody>
            <EditForm itemId={this.props.itemId} />
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }

}

export default Component;
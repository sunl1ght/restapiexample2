import React from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { createFunc } from '../actions';
import { connect } from 'react-redux';


class Component extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
      'modal': false
		};
	}

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const form = event.currentTarget;

    this.props.createFunc({
      'name': form.elements.name.value,
      'salary': form.elements.salary.value,
      'age': form.elements.age.value
    }).then(() => this.setState({'modal': false}));
  }

  render() {
    return (
      <div>
        <Button onClick={this.toggle}>Add</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle}>
          <Form onSubmit={e => this.handleSubmit(e)}>
            <ModalHeader>Add New</ModalHeader>
              <ModalBody>
                <Form.Group controlId="exampleForm.name">
                  <Form.Label>Name</Form.Label>
                  <Form.Control type="text" name="name" placeholder="Name" />
                </Form.Group>
                <Form.Group controlId="exampleForm.salary">
                  <Form.Label>Salary</Form.Label>
                  <Form.Control type="text" name="salary" placeholder="Salary" />
                </Form.Group>
                <Form.Group controlId="exampleForm.age">
                  <Form.Label>Age</Form.Label>
                  <Form.Control type="text" name="age" placeholder="Age" />
                </Form.Group>
              </ModalBody>
              <ModalFooter>
                <Button color="primary" type="submit">Submit</Button>
                <Button color="secondary" onClick={this.toggle}>Cancel</Button>
              </ModalFooter>
            </Form>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    createFunc:(data) => dispatch(createFunc(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Component);
import React from 'react';

import { deleteFunc } from '../actions';
import { connect } from 'react-redux';

import Edit from './edit';
import Delete from './delete';

class Component extends React.Component {

	constructor(props) {
		super(props);
	}

  delete = (id) => {
    if (window.confirm('Delete record Nr. ' + id + '?')) {
      this.props.deleteFunc(id);
    }
  }

  render() {
    return (
      <tr key={this.props.item}>
        <td>{this.props.item}</td>
        <td>{this.props.employees[this.props.item].employee_name}</td>
        <td>{this.props.employees[this.props.item].employee_salary}</td>
        <td>{this.props.employees[this.props.item].employee_age}</td>
        <td>
          <Edit itemId={this.props.item} />
          <Delete itemId={this.props.item} onPress={this.delete} />
        </td>
    </tr>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    employees: state.employees
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    deleteFunc:(id) => dispatch(deleteFunc(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Component);
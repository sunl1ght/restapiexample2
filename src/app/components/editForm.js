import React from 'react';
import Button from 'react-bootstrap/Button';
import { Field, reduxForm } from 'redux-form';

import { updateFunc } from '../actions';
import { connect } from 'react-redux';

class Component extends React.Component {

	constructor(props) {
		super(props);
	}

  render() {
    return (
      <form onSubmit={this.props.handleSubmit(this.props.updateFunc)}>
        <div>
            <label htmlFor="Name">Name</label>
            <Field id="employee_name{this.props.itemId}" name="employee_name" component="input" type="text" />
        </div>
        <div>
            <label htmlFor="Salary">Salary</label>
            <Field id="employee_salary{this.props.itemId}" name="employee_salary" component="input" type="text" />
        </div>
        <div>
            <label htmlFor="Age">Age</label>
            <Field id="employee_age{this.props.itemId}" name="employee_age" component="input" type="text" />
        </div>
        <Button color="primary" type="submit">Submit</Button>
    </form>
    );
  }

}

let InitializeFromStateForm = reduxForm({
  enableReinitialize: true,
  form: 'edit'
})(Component)

const mapStateToProps = (state, ownProps) => {
  return {
      initialValues: { ...state.employees[ownProps.itemId] }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
      updateFunc:(data) => dispatch(updateFunc(data))
  };
};

InitializeFromStateForm = connect(mapStateToProps, mapDispatchToProps)(InitializeFromStateForm);

export default InitializeFromStateForm;
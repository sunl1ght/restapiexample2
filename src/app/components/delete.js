import React from 'react';
import Button from 'react-bootstrap/Button';

class Component extends React.PureComponent {

	constructor(props) {
		super(props);
  }

  onPress = () => this.props.onPress(this.props.itemId);

  render() {
    return (
      <Button onClick={this.onPress}>Delete</Button>
    );
  }

}

export default Component;
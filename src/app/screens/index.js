import React from 'react';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Form from 'react-bootstrap/Form';

import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { employeesFunc } from '../actions';
import { connect } from 'react-redux';

// import AddButton from './editForm';
// import EditButton from './editForm';
// import EditForm from './editForm';

import Add from '../components/add';
import Row from '../components/row';

class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
      'loading': false
		};
	}

	componentDidMount() {
    this.setState({'loading': true});
    this.props.employeesFunc().then(() => this.setState({'loading': false}));
  }

  render() {
    return (
      <div className="App">
        <Add />
        {this.state.loading ? (
          <p>loading...</p>
        ) : (
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Salary</th>
              <th>Age</th>
              <th><br/></th>
            </tr>
          </thead>
          <tbody>
            {Object.keys(this.props.employees).map((item) => <Row key={item} item={item} />)}
          </tbody>
        </Table>
        )}
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    employees: state.employees
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    employeesFunc:() => dispatch(employeesFunc())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
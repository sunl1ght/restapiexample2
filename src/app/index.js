import React from 'react';
import { Store } from './reducers';
import { Provider } from 'react-redux';

import Main from './screens';

class App extends React.Component {
	
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Provider store={Store}>
				<Main />
			</Provider>
		);
	}
}

export default App;